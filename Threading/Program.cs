﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Threading
{
    public class Child
    {
        public static int MessageLimit = 20;
        private int _messageCount;
        private int _delay;
        private string _message;

        private object _threadLock = new object();

        public Child(string message, int delay, int messageCount)
        {
            _messageCount = messageCount;
            _delay = delay;
            _message = message;

            Thread t = new Thread(new ThreadStart(Talk));
            Console.WriteLine("Тред {0}: сообщение: {1}, лимит: {2}, задержка: {3} мс.", t.ManagedThreadId, message,
                _messageCount, _delay);
            t.Start();
        }

        private void Talk()
        {
            for (int i = 0; i < _messageCount; ++i)
            {
                if (MessageLimit <= 0)
                {
                    lock (_threadLock)
                    {
                        Console.WriteLine("Тред {0}: достигнут общий лимит, выключаюсь...",
                            Thread.CurrentThread.ManagedThreadId);
                    }

                    Thread.CurrentThread.Abort();
                }

                lock (_threadLock)
                {
                    Console.WriteLine(
                        $"Тред {Thread.CurrentThread.ManagedThreadId} ({i + 1}/{_messageCount}) -> \t{_message}");
                    --MessageLimit;
                }

                Thread.Sleep(_delay);
            }

            lock (_threadLock)
            {
                Console.WriteLine("Тред {0}: выключаюсь...", Thread.CurrentThread.ManagedThreadId);
            }
        }
    }

    public class Program
    {
        static void Main(string[] args)
        {
            int threadCount = 3;
            int localLimit = 10;
            int maxDelay = 2000;
            List<string> messages = new List<string>{ "мама", "папа" };

            try
            {
                int _threadCount = int.Parse(args[0]);
                int _localLimit = int.Parse(args[1]);
                int _messageLimit = int.Parse(args[2]);
                int _maxDelay = int.Parse(args[3]);
                if (_threadCount <= 0 || _localLimit <= 0 || _messageLimit <= 0 || _maxDelay <= 0)
                    throw new Exception();
                threadCount = _threadCount;
                localLimit = _localLimit;
                Child.MessageLimit = _messageLimit;
                maxDelay = _maxDelay;
                if (args.Length > 4)
                    messages = new List<string>();
                for (int i = 4; i < args.Length; i++)
                {
                    messages.Add(args[i]);
                }
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Использование: ./{System.AppDomain.CurrentDomain.FriendlyName} КОЛ-ВО_ТРЕДОВ ЛИМИТ_СООБЩ ОБЩИЙ_ЛИМИТ МАКС_ЗАДЕРЖКА [СООБЩ1 СООБЩ2]");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Частично используются стандартные параметры");
                Console.ForegroundColor = ConsoleColor.White;
            }

            Console.WriteLine("Мама-папа, общий лимит: {0}, тредов: {1}", Child.MessageLimit, threadCount);


            Random rand = new Random();

            for (int i = 0; i < threadCount; i++)
            {
                new Child(messages[rand.Next(messages.Count)], rand.Next(maxDelay), localLimit);
            }
        }
    }
}