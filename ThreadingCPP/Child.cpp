#include "Child.h"

std::mutex Child::sMutex;

/**
 * \param message cообщение, которое будет выводится
 * \param delay задержка в миллисекундах
 * \param message_count количество сообщений
 */
Child::Child(const std::string& message, int delay, int message_count) : mMessage(message),
                                                                         mDelay(delay),
                                                                         mMessageCount(message_count) {
}

void Child::operator()() const {
	std::unique_lock<std::mutex> tmpLock(sMutex);
	std::cout << "Тред " << std::this_thread::get_id()
		<< ":\tсообщение: " << mMessage << ",\tзадержка: " << mDelay
		<< ",\tлимит: " << mMessageCount << std::endl;
	tmpLock.unlock();
	for (int i = 0; i < mMessageCount; ++i) {
		std::this_thread::sleep_for(std::chrono::milliseconds(mDelay));
		if (sMessageLimit <= 0) {
			std::lock_guard<std::mutex> lock(sMutex);
			std::cout << "Тред " << std::this_thread::get_id() << ": Достигнут общий лимит, выключаюсь..." << std::endl;
			return;
		}

		std::lock_guard<std::mutex> lock(sMutex);
		std::cout << "Тред " << std::this_thread::get_id() << " (" << i + 1 << "/" << mMessageCount << ") \t"
			<< mMessage
			<< std::endl;
		--sMessageLimit;
	}
	std::lock_guard<std::mutex> lock(sMutex);
	std::cout << "Тред " << std::this_thread::get_id()
		<< ": Выключаюсь..." << std::endl;
}
