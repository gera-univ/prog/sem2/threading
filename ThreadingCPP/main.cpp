#include <iostream>
#include <vector>


#include "Child.h"

int Child::sMessageLimit = 20;


int main(int argc, char* argv[]) {
	int nThreads = 3;
	int localLimit = 10;
	int maxDelay = 500;
	std::vector<std::string> messages = {"мама", "папа"};
	
	try {
		if (argc < 5)
			throw std::exception();
		int _nThreads = atoi(argv[1]);
		int _localLimit = atoi(argv[2]);
		int _sMessageLimit = atoi(argv[3]);;
		int _maxDelay = atoi(argv[4]);
		if (_nThreads <= 0 || _localLimit <= 0 || _sMessageLimit <= 0 || _maxDelay <= 0)
			throw std::exception();
		nThreads = _nThreads;
		localLimit = _localLimit;
		Child::sMessageLimit = _sMessageLimit;
		maxDelay = _maxDelay;//
		if (argc > 5)
			messages.clear();
		for (int i = 5; i < argc; ++i) {
			messages.push_back(argv[i]);
		}
	} catch(...) {
		std::cerr << "Испольование: " << argv[0] << " КОЛ-ВО_ТРЕДОВ ЛИМИТ_СООБЩ ОБЩИЙ_ЛИМИТ МАКС_ЗАДЕРЖКА [СООБЩ1 СООБЩ2]\n"
		<< "Используются стандартные параметры" << std::endl;
	}

	std::cout << "Мама-папа, общий лимит: " << Child::sMessageLimit << " тредов: " << nThreads << std::endl;
	std::vector<std::thread> threads;

	for (int i = 0; i < nThreads; ++i) {
		threads.push_back(std::thread{
			Child(messages[rand() % messages.size()], rand() % maxDelay, localLimit)
		});
	}
	for (int j = 0; j < nThreads; ++j) {
		threads[j].join();
	}

}
