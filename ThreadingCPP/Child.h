#pragma once
#include <string>
#include <iostream>
#include <mutex>

class Child {
public:
	static int sMessageLimit;
	Child(const std::string& message, int delay, int message_count);

	void operator()() const;

private:
	std::string mMessage;
	int mDelay;
	int mMessageCount;
	static std::mutex sMutex;
};
